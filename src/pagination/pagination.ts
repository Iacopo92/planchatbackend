export class Pagination<T> {
	public totalPages: number;
	constructor(public totalElements: number,public pageSize: number, public currentPage: number,public content: Array<T>) {
		this.totalPages = Math.ceil(totalElements / pageSize);
	}
}
