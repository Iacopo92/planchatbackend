import { IsNumber } from "class-validator";
import { Type, Transform } from "class-transformer";
import { createParamDecorator } from "@nestjs/common";

export class PageRequest {
	constructor(public page: number, public size: number) {
	}
}

export const Page = createParamDecorator(async (data, req) => {
	const query = req.query;
	const page = query.page ? Number(query.page) : 0;
	const size = query.size ? Number(query.size) : 20;
	return new PageRequest(page, size);
});
