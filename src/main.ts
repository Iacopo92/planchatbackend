import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { AuthGuard } from '@nestjs/passport';

async function bootstrap() {
	const app = await NestFactory.create(AppModule);
	app.enableCors({
		methods: '*',
		allowedHeaders: '*',
		optionsSuccessStatus: 200
	});

	await app.listen(3000);
}
bootstrap();
