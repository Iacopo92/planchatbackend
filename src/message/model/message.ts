
import { Document } from 'mongoose';
import { User } from 'users/model/user';

export interface Message extends Document {
	from: User;
	text: string;
	timestamp: Date;
}
