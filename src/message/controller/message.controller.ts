import { Controller, Get, Param, UseGuards, BadRequestException, Query, Post, Body } from '@nestjs/common';
import { MessageDTO } from '../dto/message.dto';
import { MessageService } from '../service/message.service';
import { MessageMapper } from '../mapper/message.mapper';
import { Pagination } from 'pagination/pagination';
import { PageRequest, Page } from '../../pagination/page-request';
import { JwtAuthGuard } from '../../auth/service/jwt-auth-guard';
import { LoggedUser } from '../../auth/logged-user.pipe';
import { User } from './../../users/model/user';
import {WebSocketGateway, SubscribeMessage} from '@nestjs/websockets';

@UseGuards(new JwtAuthGuard())
@Controller('/api/messages')
export class MessageController {
	constructor(private readonly messageService: MessageService, private readonly messageMapper: MessageMapper) {}

	@Get()
	public async getBetweenDates(@Query('endDate') endDate: string, @Page() page: PageRequest): Promise<Pagination<MessageDTO>> {
		const messages = await this.messageService.findByEndDate(new Date(endDate), page);
		return this.messageMapper.pageToPageDTO(messages);
	}

	@Post()
	public async addMessage(@Body() body: any, @LoggedUser() user: User): Promise<MessageDTO> {
		const message = await this.messageService.insert(body.message, user);
		return this.messageMapper.modelToDTO(message);
	}
}
