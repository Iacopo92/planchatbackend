import { UseGuards } from '@nestjs/common';
import { MessageDTO } from '../dto/message.dto';
import { MessageService } from '../service/message.service';
import { MessageMapper } from '../mapper/message.mapper';
import { JwtAuthGuard } from '../../auth/service/jwt-auth-guard';
import { LoggedUser } from '../../auth/logged-user.pipe';
import { User } from 'users/model/user';
import {WebSocketGateway, SubscribeMessage, WebSocketServer} from '@nestjs/websockets';

@WebSocketGateway({namespace: 'messages'})
export class MessageSocketController {

  @WebSocketServer() server;
	constructor(private readonly messageService: MessageService, private readonly messageMapper: MessageMapper) {}

	@SubscribeMessage('add')
	@UseGuards(new JwtAuthGuard())
	public async onMessage(client, message: string, @LoggedUser() user: User) {
    console.log('ricevuto messaggio');
		const createdMessage = await this.messageService.insert(message, client.user);
    const dto = this.messageMapper.modelToDTO(createdMessage);
    this.server.emit('message', dto);
	}
}

