import { MessageDTO } from "../dto/message.dto";
import { Injectable } from "@nestjs/common";
import { Message } from "../model/message";
import { Pagination } from "../../pagination/pagination";
import { UserMapper } from "../../users/mapper/user.mapper";

@Injectable()
export class MessageMapper {
	constructor(private userMapper: UserMapper) {}

	modelToDTO(message: Message): MessageDTO {
		if(!message) return null;
		const dto = new MessageDTO();
		dto.id = message.id;
		dto.from = this.userMapper.modelToDTO(message.from);
		dto.message = message.text;
		dto.timestamp = message.timestamp;
		return dto;
	}

	listToDTOs(messages: Message[]) : MessageDTO[] {
		if(!messages) return null;
		return messages.map(u => this.modelToDTO(u));
	}

	pageToPageDTO(page: Pagination<Message>): Pagination<MessageDTO> {
		return new Pagination<MessageDTO>(page.totalElements, page.pageSize, page.currentPage, this.listToDTOs(page.content));
	}
}
