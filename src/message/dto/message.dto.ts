import { Expose, Type } from 'class-transformer';
import { UserDTO } from 'users/dto/user.dto';

export class MessageDTO {
	id: string;
	from: UserDTO;
	message: string;
	timestamp: Date;
}
