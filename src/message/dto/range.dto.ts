import * as moment from "moment";
import { Type, Transform } from "class-transformer";
import { Moment } from "moment";

export class RangeDTO {
	@Type(() => Date)
	@Transform(value => moment(value), { toClassOnly: true })
	start: Moment;

	@Type(() => Date)
	@Transform(value => moment(value), { toClassOnly: true })
	end: Moment;
}
