import { IsNotEmpty, MinLength, IsString, IsAlphanumeric, IsEmail } from 'class-validator';

export class CreateMessageDTO {
	@IsNotEmpty()
	@IsString()
	public message: string;
}
