import { Injectable, ConflictException } from '@nestjs/common';
import { InjectModel } from '@nestjs/mongoose';
import { MessageDao } from '../dao/message.dao';
import { Message } from '../model/message';
import { JwtPayload } from 'auth/model/logged-user';
import { User } from 'users/model/user';
import { Moment } from 'moment';
import { PageRequest } from '../../pagination/page-request';
import { Pagination } from '../../pagination/pagination';
import { isNullOrUndefined } from 'util';
@Injectable()
export class MessageService {

	constructor(
		@InjectModel('Message') private readonly messageDao: MessageDao) {}

	public async insert(text: string, user: User): Promise<Message> {
		if (isNullOrUndefined(text) || text.trim().length === 0) throw new Error('empty message');
		return await this.messageDao.create({
			from: user,
			text,
			timestamp: new Date()
		});
	}

	public async findByEndDate(endDate: Date,page: PageRequest): Promise<Pagination<Message>> {
		const baseQuery = this.messageDao.find({
			timestamp: {
				$lte: endDate
			}
		});
		const content = await baseQuery.populate('from').sort({timestamp: 'desc'}).skip(page.size * page.page).limit(page.size).exec();
		const size = await baseQuery.countDocuments();
		return new Pagination<Message>(size, page.size, page.page, content);
	}
}
