import { Model } from "mongoose";
import { Message } from "message/model/message";

export interface MessageDao extends Model<Message> {}
