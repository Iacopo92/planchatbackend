import { Module, NestModule, MiddlewareConsumer } from '@nestjs/common';
import { MessageController } from './controller/message.controller';
import { MessageService } from './service/message.service';
import { MessageMapper } from './mapper/message.mapper';
import { MongooseModule } from '@nestjs/mongoose';
import { MessageSchema } from './message.schema';
import { PassportModule } from '@nestjs/passport';
import { UsersModule } from '../users/users.module';
import { MessageSocketController } from './controller/message.socket-controller';

@Module({
	imports: [MongooseModule.forFeature([{ name: 'Message', schema: MessageSchema }]), UsersModule, PassportModule],
	controllers: [MessageController],
	providers: [MessageService, MessageMapper, MessageSocketController]
})
export class MessageModule{}
