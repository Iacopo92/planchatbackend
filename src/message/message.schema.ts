import * as mongoose from 'mongoose';

export const MessageSchema = new mongoose.Schema({
	from: { type: mongoose.Schema.Types.ObjectId, ref: 'User'},
	text: String,
	timestamp: {type: Date, index: true},
}, {collection: 'messages'});
