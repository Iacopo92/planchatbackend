import { Injectable, UnauthorizedException } from '@nestjs/common';
import { AuthService } from './auth.service';
import { ExtractJwt, Strategy, JwtFromRequestFunction } from 'passport-jwt';
import { PassportStrategy } from '@nestjs/passport';
import { JwtPayload } from '../model/logged-user';

export function fromSocketHandshake(): JwtFromRequestFunction {
	return (request: any) => {
		try {
			let aut: string = request.handshake.query.Authorization;
			return aut.slice('Bearer '.length);
		} catch(error) {
			return null;
		}
	}
}

@Injectable()
export class JwtStrategyService extends PassportStrategy(Strategy){
	constructor(private readonly authService: AuthService) {
		super({
			jwtFromRequest: ExtractJwt.fromExtractors([fromSocketHandshake(), ExtractJwt.fromAuthHeaderAsBearerToken()]),
			secretOrKey: 'secretKey',
		});
	}

	async validate(payload: JwtPayload) {
		const user = await this.authService.validateUser(payload);
		if (!user) {
			throw new UnauthorizedException();
		}
		return user;
	}
}
