import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { JwtPayload } from '../model/logged-user';
import { UserService } from '../../users/service/user.service';

@Injectable()
export class AuthService {
	constructor(
		private readonly userService: UserService,
		private readonly jwtService: JwtService,
  ) {}

	async logIn(email: string, password: string): Promise<string> {
		const user = await this.userService.findUserByEmail(email.toLowerCase());
		if (!user || user.password !== password) throw new UnauthorizedException('UNAUTHORIZED');
		const payload: JwtPayload = { id: user.id, email: user.email, username: user.username };
		const expire = 3600;
		return this.jwtService.sign(payload, {expiresIn: expire});
	}

	async validateUser(payload: JwtPayload): Promise<any> {
		return this.userService.findOne(payload.id);
	}
}
