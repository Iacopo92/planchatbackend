import { ArgumentMetadata, Pipe, PipeTransform } from '@nestjs/common';

import { createParamDecorator } from '@nestjs/common';

export const LoggedUser = createParamDecorator(async (data, req) => {
	return req.user;
});
