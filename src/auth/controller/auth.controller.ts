import { Controller, Post, Body, HttpCode , Response, Header} from '@nestjs/common';
import { AuthService } from '../service/auth.service';
import { CreateUserDTO } from '../../users/dto/create-user.dto';
import { UserService } from '../../users/service/user.service';
import { UserMapper } from '../../users/mapper/user.mapper';
import { UserDTO } from '../../users/dto/user.dto';
import { LoginRequest } from '../model/login-request';

@Controller('auth')
export class AuthController {
	constructor(
		private readonly authService: AuthService,
		private readonly userService: UserService,
		private readonly userMapper: UserMapper) {}

	@Post('token')
	public async login(@Body() loginRequest: LoginRequest) {
		const token = await this.authService.logIn(loginRequest.email, loginRequest.password);
		return {token};
	}

	@Post('signin')
	async register(@Body() createUser: CreateUserDTO): Promise<UserDTO> {
		const user = await this.userService.createUser(createUser.email, createUser.password, createUser.username);
		return this.userMapper.modelToDTO(user);
	}
}
