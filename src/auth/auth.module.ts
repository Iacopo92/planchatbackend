import { Module, NestModule, MiddlewareConsumer, RequestMethod, UnauthorizedException, Middleware, NestMiddleware, MiddlewareFunction } from '@nestjs/common';
import { AuthService } from './service/auth.service';
import { JwtStrategyService } from './service/jwt-strategy.service';
import { PassportModule } from '@nestjs/passport';
import { JwtModule } from '@nestjs/jwt';
import { AuthController } from './controller/auth.controller';
import { UsersModule } from '../users/users.module';
import { JwtAuthGuard } from './service/jwt-auth-guard';

@Module({
	imports: [
		PassportModule.register({ defaultStrategy: 'jwt',session: false }),
		JwtModule.register({
			secretOrPrivateKey: 'secretKey',
		}),
		UsersModule,
],
	controllers: [AuthController],
	providers: [AuthService, JwtStrategyService, JwtAuthGuard],
	exports: [PassportModule, JwtStrategyService, AuthService, JwtAuthGuard]
})
export class AuthModule {}
