import { Module, ValidationPipe } from '@nestjs/common';
import { AppService } from './app.service';
import { MongooseModule } from '@nestjs/mongoose';
import { APP_PIPE } from '@nestjs/core';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { PassportModule } from '@nestjs/passport';
import { MessageModule } from './message/message.module';
const docker = 'mongodb://localhost:32769/plan-chat';
const mlab = 'mongodb://plan:plangroup1@ds113003.mlab.com:13003/plan-chat';
@Module({
  imports: [
		MongooseModule.forRoot(mlab, {useNewUrlParser: true}),
		UsersModule,
		AuthModule,
		PassportModule,
		MessageModule],
	providers: [
		{provide: APP_PIPE, useClass: ValidationPipe }
	]
})
export class AppModule {}
