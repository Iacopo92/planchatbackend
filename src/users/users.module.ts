import { Module } from '@nestjs/common';
import { UserSchema } from './user.schema';
import { MongooseModule } from '@nestjs/mongoose';
import { UserService } from './service/user.service';
import { UserController } from './controller/user.controller';
import { UserMapper } from './mapper/user.mapper';
import { PassportModule } from '@nestjs/passport';

@Module({
	imports: [MongooseModule.forFeature([{ name: 'User', schema: UserSchema }]), PassportModule],
	providers: [UserService, UserMapper],
	controllers: [UserController],
	exports: [UserService, UserMapper]
})
export class UsersModule {}
