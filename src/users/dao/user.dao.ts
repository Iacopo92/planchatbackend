import { Model } from "mongoose";
import { User } from "users/model/user";

export interface UserDao extends Model<User> {}
