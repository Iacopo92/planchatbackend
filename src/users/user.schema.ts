import * as mongoose from 'mongoose';

export const UserSchema = new mongoose.Schema({
  email: {type: String, index: true },
	username: String,
  password: String
}, {collection: 'users'});
