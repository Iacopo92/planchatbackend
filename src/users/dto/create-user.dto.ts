import { IsNotEmpty, MinLength, IsString, IsAlphanumeric, IsEmail } from 'class-validator';

export class CreateUserDTO {

	@IsNotEmpty()
	@IsString()
	public username: string;

	@IsNotEmpty()
	@IsString()
	@IsEmail()
	public email: string;

	@MinLength(8)
	@IsNotEmpty()
	@IsString()
	public password: string;
}
