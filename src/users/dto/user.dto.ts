import { Expose, Type } from 'class-transformer';

export class UserDTO {
	id: string;
	username: string;
	email: string;
}
