import { UserDTO } from "../dto/user.dto";
import { User } from "../model/user";
import { Injectable, HttpStatus } from "@nestjs/common";

@Injectable()
export class UserMapper {

	modelToDTO(user: User): UserDTO {
		if(!user) return null;
		const userDTO = new UserDTO();
		userDTO.id = user.id;
		userDTO.username = user.username;
		userDTO.email = user.email;
		return userDTO;
	}

	listToDTOs(users: User[]) : UserDTO[] {
		if(!users) return null;
		return users.map(u => this.modelToDTO(u));
	}
}
