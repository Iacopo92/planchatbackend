import { Injectable, ConflictException } from '@nestjs/common';
import { User } from '../model/user';
import { InjectModel } from '@nestjs/mongoose';
import { Model } from 'mongoose';
import { UserDao } from '../dao/user.dao';

@Injectable()
export class UserService {

	constructor(@InjectModel('User') private readonly userDao: UserDao) {}

	public async createUser(email: string, password: string, username: string): Promise<User> {
		const user = await this.findUserByEmail(email);

		if (user) {
			throw new ConflictException('USER.CREATE.ALREADY-EXIST');
		}

		return await this.userDao.create({
			email: email.toLowerCase().trim(),
			username,
			password
		});
	}

	public findUserByEmail(email: string): Promise<User> {
		return this.userDao.findOne({email: email.toLowerCase().trim()}).exec();
	}

	public findAll(): Promise<User[]> {
		return this.userDao.find().exec();
	}

	public findOne(id: string): Promise<User> {
		return this.userDao.findById(id).exec();
	}
}
