import { Controller, Post, Body, Get, UseGuards, Param, BadRequestException } from '@nestjs/common';
import { UserService } from '../service/user.service';
import { UserMapper } from '../mapper/user.mapper';
import { UserDTO } from '../dto/user.dto';
import { LoggedUser } from './../../auth/logged-user.pipe';
import { User } from '../model/user';
import { JwtAuthGuard } from '../../auth/service/jwt-auth-guard';
import { isNullOrUndefined } from 'util';

@UseGuards(new JwtAuthGuard())
@Controller('api/users')
export class UserController {
	constructor(private userService: UserService, private userMapper: UserMapper) {}

	@Get()
	async get(): Promise<UserDTO[]> {
		const users = await this.userService.findAll();
		return this.userMapper.listToDTOs(users);
	}

	@Get(':id')
	async getById(@Param('id') id: string, @LoggedUser() loggedUser: User): Promise<UserDTO> {
		const user = id.toLowerCase() === 'me' ? loggedUser : await this.userService.findOne(id);
		return this.userMapper.modelToDTO(user);
	}
}
